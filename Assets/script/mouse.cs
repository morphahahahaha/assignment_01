﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class mouse : MonoBehaviour {
    public Text scores;
    public float x = 0f;

	public float timeup = 0f;
	public float timedown = 1.5f;

    private GameObject gameManager;
    private GameObject scoreboard;

    // Use this for initialization
    void Start () {
        //  scores.text = "score :" + x;
        gameManager = GameObject.Find("gamemanger");
        scoreboard = GameObject.Find("scorebod");


    }
	
	// Update is called once per frame
	void Update () {
		Destroy(this.gameObject , 1.5f);
		timeup += Time.deltaTime;
		if (timeup >= timedown) {
			scoreboard.GetComponent<lives> ().minuslife ();
			timeup = 0f;
		}
    }

    void OnMouseDown()
    {

      //  x += 1f;
        Destroy(this.gameObject);

        gameManager.GetComponent<score>().addScore();
		scoreboard.GetComponent<lives> ().addlife();

    }

}

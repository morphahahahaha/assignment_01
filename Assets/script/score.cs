﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class score : MonoBehaviour {
    public Text scoreText;
    public int scores = 0;
	public int win = 20;


	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
     scoreText.text = "score :" + scores;
     print("Score: " + scores);

		if (scores >= win) {

			Application.LoadLevel("win");
		}
	}

    // we will call this function later when we wanna add the score //
    public void addScore() {
        scores += 1;
        print(scores);
    }

    // we will call this function later when we wanna clear the score //
    public void clearScore() {
        scores = 0;
    }

  
}

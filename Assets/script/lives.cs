﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class lives : MonoBehaviour {

    public Text livescore;
    public int live = 3;
	public int losegame = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        livescore.text = "LIVES :" + live;
		if (live <= losegame)
		{
			Application.LoadLevel("lose");
		}
	}

    void OnMouseDown()
    {
        live--;
    }
	public void addlife()
	{
		live += 1;
	}
	public void minuslife()
	{
		live -= 1;
	}
}
